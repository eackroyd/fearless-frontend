window.addEventListener('DOMContentLoaded', async () =>{
    const url = 'http://localhost:8000/api/states/';
    const response = await fetch(url);

    if (response.ok) {
        const data = await response.json();

        const selectTag = document.getElementById('state');
        for (let state of data.states){
            let {name, abbreviation} = state;
            const option = document.createElement('option');
            option.value = abbreviation;
            option.innerHTML = name;
            const select_el = document.querySelector('#state');
            select_el.append(option);
        }
    }

    const formTag = document.getElementById('create-location-form');
    formTag.addEventListener('submit', async event => {
        event.preventDefault();
        const formData = new FormData(formTag);
        const json = JSON.stringify(Object.fromEntries(formData));
        const locationUrl = "http://localhost:8000/api/locations/";
        const fetchConfig = {
            method: "POST",
            body: json,
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const location_response = await fetch(locationUrl, fetchConfig);
        if (location_response.ok) {
            formTag.reset();
            const newLocation = await location_response.json();
            console.log(newLocation);
        }
    })
})
