function createCard(name, description, pictureUrl, starts, ends, location) {
    return `
        <div class="col-4">
            <div class="card shadow p-3 mb-5 bg-white rounded" style = "card-box-shadow">
                <img src="${pictureUrl}" class="card-img-top">
                    <div class="card-body">
                    <h5 class="card-title">${name}</h5>
                    <h6 class="card-subtitle mb-2 text-body-secondary text-muted">${location}</h6>
                    <p class="card-text">${description}</p>
                </div>
                <div class="card-footer">
                    ${starts} - ${ends}
                </div>
            </div>
        </div>
    `;
  }

window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';
    try{
        const response = await fetch(url);
        if (!response.ok){
            throw new Error("Error getting data from API");
        }else {
            const data = await response.json();

            for (let conference of data.conferences){
                const detailUrl = `http://localhost:8000${conference.href}`;
                const detailResponse = await fetch(detailUrl);
                if (detailResponse.ok){
                    const details = await detailResponse.json();
                    const title = details.conference.name;
                    const description = details.conference.description;
                    const picture_url = details.conference.location.picture_url;
                    const s = details.conference.starts;
                    let s1 = new Date(s);
                    let starts = String(s1.getMonth() + 1) + "/" + String(s1.getDate()) + "/" + String(s1.getFullYear());
                    const e = details.conference.ends;
                    let e1 = new Date(e);
                    let ends = String(e1.getMonth() + 1) + "/" + String(e1.getDate()) + "/" + String(e1.getFullYear());
                    const location = details.conference.location.name;
                    const html = createCard(title, description, picture_url, starts, ends, location);
                    const row = document.querySelector('.row');
                    row.innerHTML += html;
                }
            }
        }
    } catch (e) {
        console.error(e);
    }
});
