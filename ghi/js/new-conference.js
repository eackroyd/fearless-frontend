window.addEventListener('DOMContentLoaded', async () =>{
    const url = 'http://localhost:8000/api/locations/';
    const response = await fetch(url);

    if (response.ok){
        const data = await response.json();
        const selectTag = document.getElementById('location');
        for (let location of data.locations){
            let {name, id} = location;
            console.log(location);
            const option = document.createElement('option');
            option.value = id;
            option.innerHTML = name;
            const select_el = document.querySelector('#location');
            select_el.append(option);
        }
    }

    const formTag = document.getElementById('create-conference-form');
    formTag.addEventListener('submit', async event => {
        event.preventDefault();
        const formData = new FormData(formTag);
        const json = JSON.stringify(Object.fromEntries(formData));
        const conferenceUrl = "http://localhost:8000/api/conferences/";
        const fetchConfig = {
            method: "POST",
            body: json,
            headers: {
                'Content-Type': 'application/json',
            }
        };
        const conference_response = await fetch(conferenceUrl, fetchConfig);
        if (conference_response.ok){
            formTag.reset();
            const newConference = await conference_response.json();
            console.log(newConference);
        }
    })
})
